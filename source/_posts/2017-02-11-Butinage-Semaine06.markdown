---
layout: post
title: "Mes butinages de la semaine 06"
date: 2017-02-12 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraf
tmanship, technical, debt]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 06!

## Vie privée
* [Pourquoi j'ai invité mes amis à ne plus utiliser WhatsApp](http://datanews.levif.be/ict/actualite/pourquoi-j-ai-invite-mes-amis-a-ne-plus-utiliser-whatsapp/article-opinion-607041.html)
* [Yunohost](https://linuxfr.org/news/evolutions-des-projets-la-brique-internet-et-yunohost-des-versions-2-2-2-4-et-2-5)

## Agilité
* [Sprint planning](http://www.aubryconseil.com/post/Les-6-activites-de-planification-du-sprint)

## Divers
* [Un hébergeur du Dark Web attaqué par Anonymous](http://homputersecurity.com/2017/02/06/un-hebergeur-du-dark-web-attaque-par-anonymous/)  
* [L'intelligence artificielle de Google recompose une photo comme dans Les Experts](http://www.lefigaro.fr/secteur/high-tech/2017/02/09/32001-20170209ARTFIG00014-l-intelligence-artificielle-de-google-recompose-une-photo-comme-dans-les-experts.php)