---
layout: post
title: "Mes butinages de la semaine 10"
date: 2017-03-12 00:42:00 +0000
comments: true
categories: [vie privée, liberté, softwarecraftmanship]
---


**Bonne butinage !**

# Butinages de la semaine 10!

## Artisanant logiciel
* [Secrets of Maintainable Codebases ](https://dzone.com/articles/secrets-of-maintainable-codebases)
* [Demain, les développeurs… ?](https://framablog.org/2017/03/06/demain-les-developpeurs/)
* [East-Oriented Programming](http://www.draconianoverlord.com/2013/04/12/east-oriented-programming.html)

## Vie privée
* [Le «fichier des gens honnêtes», ce révélateur d'un mal français](http://www.slate.fr/story/138356/saga-generalisation-fichier-des-gens-honnetes)

## Divers
* [Ce que peut réellement Memex, le moteur de recherche du web profond américain](http://www.diplomatie-digitale.com/featured/surete/memex-web-profond-darpa-1534)
* [Raspberry Pi : regarder une vidéo 4K sur sa télévision](http://alexandre-laurent.developpez.com/articles/hardware/raspberry-pi/video-film-4K/)
