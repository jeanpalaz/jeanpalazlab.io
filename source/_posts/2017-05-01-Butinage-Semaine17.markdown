---
layout: post
title: "Mes butinages de la semaine 17"
date: 2017-05-01 00:42:00 +0000
comments: false
categories: [agilité, éducation, liberté, softwarecraftmanship, climate change, ted]
---

** Bon butinage !**

## Artisanat logiciel
* [Quels pourraient être les archétypes du hacker ?](https://www.developpez.com/actu/129646/Quels-pourraient-etre-les-archetypes-du-hacker-Eric-Raymond-un-gourou-de-l-open-source-suggere-une-liste-pour-motiver-les-debutants/)

## Internet
* [La FCC amorce la fin de la neutralité du Net](http://www.silicon.fr/fcc-amorce-fin-neutralite-du-net-173343.html)
* [Premier essai (raté) de raccordement à la Fibre](https://www.bortzmeyer.org/premier-essai-fibre.html)

## Éducation - Agile
* [SIRHEN : les méthodes agiles peuvent-elles sauver le projet fou de l’Education Nationale ?](http://www.silicon.fr/sirhen-methodes-agiles-sauver-projet-fou-education-nationale-173299.html)

## Divers
* [Lawrence Lessig : « Le problème de la démocratie actuellement c’est qu’elle n’est pas représentative »](http://www.lemonde.fr/pixels/article/2017/04/23/le-principal-probleme-de-la-democratie-actuellement-c-est-qu-elle-n-est-pas-representative_5115774_4408996.html)
* [How to Hack Your Own Linux System](http://www.tecmint.com/how-to-hack-your-own-linux-system/)
* [The art of asking](https://www.ted.com/talks/amanda_palmer_the_art_of_asking)
* [Friendly Guide to Climate Change - and what you can do to help #everytoncounts](https://youtu.be/3CM_KkDuzGQ)
