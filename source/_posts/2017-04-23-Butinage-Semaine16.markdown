---
layout: post
title: "Mes butinages de la semaine 16"
date: 2017-04-23 00:42:00 +0000
comments: false
categories: [vieprivée, liberté, softwarecraftmanship, gratitude, éducation]
---

** Bon butinage !**

## Artisanat logiciel
* [Plongée dans le dark code](https://www.infoq.com/fr/news/2017/04/plongee-dans-le-dark-code)
* [Code It Yourself](https://shirleyalmosni.wordpress.com/2016/09/24/code-it-yourself/)

## Sécurité, vie privée
* [es navigateurs Opera, Chrome, Firefox, vulnérables à cette attaque d’hameçonnage](http://homputersecurity.com/2017/04/18/les-navigateurs-opera-chrome-firefox-vulnerables-cette-attaque-dhameconnage/)

## Éducation
* [How to Foster Gratitude in Schools](http://greatergood.berkeley.edu/article/item/how_to_foster_gratitude_in_schools)