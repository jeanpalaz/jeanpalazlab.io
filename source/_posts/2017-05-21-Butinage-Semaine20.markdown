---
layout: post
title: "Mes butinages de la semaine 20"
date: 2017-05-21 00:42:00 +0000
comments: false
categories: [agilité, liberté, softwarecraftmanship, cleancode, wannacry, intelligence-émotionnelle]
---

** Bon butinage !**

## Artisanat logiciel
* [The Programmer and the Pulpit](https://dev.to/t0ss_games/the-programmer-and-the-pulpit)
* [What is Clean Code and why should you care?](https://dev.to/cvuorinen/what-is-clean-code-and-why-should-you-care)
* [Error handling - Returning Results ](https://dev.to/michael_altmann/error-handling---returning-results)

## Podcast
* [NoLimitSecu - WannaCry](https://www.nolimitsecu.fr/wannacry/)
* [CPU - Ex0053 Fantômes de la déconnexion](https://cpu.dascritch.net/post/2017/04/06/Ex0053-Fant%C3%B4mes-de-la-d%C3%A9connexion)
* [CPU - Ex0054 Bugs, failles et trous de sécurité](https://cpu.dascritch.net/post/2017/05/18/Ex0054-Bugs%2C-failles-et-trous-de-s%C3%A9curit%C3%A9)

## Éducation - Agile
* [Management - Se réinventer grâce à l’intelligence émotionnelle](http://www.hbrfrance.fr/chroniques-experts/2017/05/15511-se-reinventer-grace-a-lintelligence-emotionnelle/)
