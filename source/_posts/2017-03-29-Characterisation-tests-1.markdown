---
layout: post
title: "Characterisation tests - 1er partie"
date: 2017-03-29 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraftmanship, technical, debt]
---

# les Tests de Caractérisation (1er partie)

## Introduction
  Nous, artisans développeurs, travaillons tous avec du vieux code souvent obscur, sans intention explicite, et surtout non testé.
La question qui se pose est comment le maintenir, le faire évoluer sans risque, et aussi le remanier (refactorer en franglais).
Le code existant ou vieux code est aussi appelé "Legacy code", ce terme a été introduit par Michael Feathers. La traduction de 'Legacy' n'est pas si évidente, on entend beaucoup le mot héritage mais ce n'est pas bon. 'Legacy' n'est pas à prendre dans le sens d'héritage, mais bien de [dette](https://dzone.com/articles/technical-liabilities-not-technical-debt?edition=260684&utm_source=Daily%20Digest&utm_medium=email&utm_campaign=dd%202016-12-27). On parle donc de dette technique (["Technical Debt" par Ward Cunningham](http://wiki.c2.com/?WardExplainsDebtMetaphor)).

  Alors comment modifier un code issue de la dette technique qui n'a pas de tests ?
Une première idée serait d'écrire les tests à partir des spécifications (specification tests). Or dans cette approche on va mettre en avant surtout ce que doit faire le code ou ce qu'il ne fait pas...
Mais est-ce que la spécification est à jour? Or si on veut être pragmatique, le service rendu par le logiciel est celui fait par le code et non celui écrit dans la spécification. Ici on considère que le code existant est à priori correct dans ce qu'il fait. Et donc en l'absence de test, toutes modifications de code ne doivent pas dénaturer le comportement du logiciel.

## Alors comment faire?
  Il faut commencer par écrire des tests qui sont représentatifs du comportement du code actuel. On dit que l'on cherche à caractériser le code.
On va donc écrire des tests de caractérisation [characterization tests](https://michaelfeathers.silvrback.com/characterization-testing). A partir de ces tests qui passent, on peut donc modifier/refactorer le code avec un filet de sécurité. Si la modification laisse le test vert, on peut continuer, rien n'est cassé.
Lire le code testé va servir non pas pour acquérir la compréhension de ce qu'il fait, mais à comprendre quoi tester.

- Choisir un bout de code
- Ecrire un test qui échoue (rouge)
- Le résultat retourné va servir de référence, on a une caractérisation \o/
- Modifier le test en conséquence, le test passe au vert
- Répéter

## Chouette mais on répète la boucle combien de fois avant de modifier le code ?

- Faire varier le(s) paramètre(s) pour obtenir plus de résultats, en s'aidant des branches de condition
- Avec un outil de couverture de code on peut trouver d'autres valeurs de paramètre afin d'augmenter la couverture
  => en peu de temps on doit pouvoir obtenir 100%
  => On peut aussi faire une approche 'Golden Master'. Cette approche consiste à dupliquer la portion de code (méthode/classe) elle devient la référence. On recode et on compare ce qu'on fait avec le code de référence --> on doit avoir le même résultat.
- A cette étape nous avons une meilleur compréhension de ce que fait le code grâce à la variation des paramètres et la lecture des branches de condition pour obtenir cette couverture.
- Le refactor peut commencer ! (http://martinsson-johan.blogspot.fr/2014/05/refactorer-legacy-meme-pas-peur.html)
  => TDD rules ! [I FLIP THE BIT !](https://8thlight.com/blog/uncle-bob/2012/01/11/Flipping-the-Bit.html), et vous?

## Pour pratiquer

- Kata Gilded Rose (https://github.com/emilybache/GildedRose-Refactoring-Kata)
- Kata TripService (https://github.com/sandromancuso/trip-service-kata)

## Quelques outils

- Approval Test: (http://approvaltests.com/)
- Infinity test: (https://infinitest.github.io/)

## **Happy Coding !**

### Liens et compléments
- "Working Effectively With Legacy Code"-Michael Feathers. (https://dzone.com/articles/technical-liabilities-not-technical-debt?edition=260684&utm_source=Daily%20Digest&utm_medium=email&utm_campaign=dd%202016-12-27)
- (http://martinsson-johan.blogspot.fr/2014/05/refactorer-legacy-meme-pas-peur.html)
- (http://www.artima.com/weblogs/viewpost.jsp?thread=198296)
- (http://www.artima.com/weblogs/viewpost.jsp?thread=198674)
- (http://www.artima.com/weblogs/viewpost.jsp?thread=198970)
- (http://www.artima.com/weblogs/viewpost.jsp?thread=200725)
- (https://8thlight.com/blog/uncle-bob/2012/01/11/Flipping-the-Bit.html)
- (http://codurance.com/2011/07/03/working-with-legacy-code/)
- (http://approvaltests.sourceforge.net/?q=node/7)
- (https://michaelfeathers.silvrback.com/characterization-testing)
- (http://craftedsw.blogspot.com/2012/11/testing-legacy-code-with-golden-master.html)
- (http://wiki.c2.com/?WardExplainsDebtMetaphor)

### PS:
* J'ai trop tardé à publier cet article, alors livrer souvent étant la clé, j'écrirai une suite...
* Merci au relecteur par [@ludopradel](https://twitter.com/ludopradel)