---
layout: post
title: "Mes butinages de la semaine 01"
date: 2017-01-09 00:42:00 +0000
comments: true
categories: [Blog, Test, code, softwarecraftmanship, leadership, inteligence]
---

Voilà, je fais part de mes 'butinations' hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyer mes lectures/vidéos à mes connaissances/amis/collègues.
Donc je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bon butinage !**

# Mes butinages de la semaine!

## Artisanat logiciel
* [Design pattern : Builder et Builder sont dans un bateau](http://blog.xebia.fr/2016/12/28/design-pattern-builder-et-builder-sont-dans-un-bateau/)
* [When A Method Can Do Nothing](https://michaelfeathers.silvrback.com/when-it-s-okay-for-a-method-to-do-nothing)
* [Gold Master Testing](http://blog.codeclimate.com/blog/2014/02/20/gold-master-testing/)
* [How Not To Write Golden Master Tests](http://blog.thecodewhisperer.com/permalink/how-not-to-write-golden-master-tests)
* [La voie du programmeur - Jean-Baptiste Dusseaut](https://www.youtube.com/watch?v=njbBjVDoE2I)
* [L'ordre des développeurs - Jean-Baptiste Dusseaut](https://www.youtube.com/watch?v=njbBjVDoE2I)

## Agilité
* [Patterns organisationnels](http://blog.xebia.fr/2017/01/04/oubliez-les-frameworks-agile-pensez-patterns/)
* C'est de saison: [Epiphanie organisationnelle](http://areyouagile.com/2016/12/epiphanie-organisationnelle/)
* [Leadership - action - Simon Sinek](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action?language=fr)
* [Leadership - bienveillant- Simon Sinek](https://www.ted.com/talks/simon_sinek_why_good_leaders_make_you_feel_safe)

## Divers
* [L'opportunité de l'adversité - Aimee Mullins](https://www.youtube.com/watch?v=dTwXeZ4GkzI)
* [L'auto-hébergement c'est bien, mais pas à la maison](https://www.athaliasoft.com/lauto-hebergement-cest-bien-mais-pas-a-la-maison)
* [Comment nous avons tous pris la mauvaise habitude d'être fatigué](http://www.slate.fr/story/133412/sommeil-enfants)
* Les intelligences multiples - Christian Philibert: [Partie 1](https://youtu.be/fsFjGby7pYc), [Partie 2](https://youtu.be/JMjCNdmyDr8), [Partie 3](https://youtu.be/Kf08iTZZyg0), [Partie 4](https://youtu.be/faUe5P8omKc), [Diapositives](http://slideplayer.fr/slide/1145191/)