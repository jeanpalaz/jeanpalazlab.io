---
layout: post
title: "Mes butinages de la semaine 08"
date: 2017-02-26 00:42:00 +0000
comments: true
categories: [agilité, liberté, softwarecraftmanship]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 08!

## Agilité
* [La Dictée Amicale (I)](https://pedagogieagile.com/2017/02/21/la-dictee-amicale-i/)
* [Est-ce que vous voulez de l’agilité de merde ?](http://www.les-traducteurs-agiles.org/agile/2016/04/27/vous-voulez-quoi.html)

## Libertés numériques
* [Facebook n’est pas un réseau social, c’est un scanner qui nous numérise](https://framablog.org/2017/02/20/facebook-scanner-qui-nous-numerise/)
* [Surveillons la surve://ance](https://framablog.org/2017/02/14/surveillons-la-surveillance/)

## Artisanat logiciel
* [Les bonnes raisons de créer ou rejoindre une SCOP](http://theconversation.com/les-bonnes-raisons-de-creer-ou-rejoindre-une-scop-71917)
* [Just Blog It](http://www.ouarzy.com/2017/01/13/just-blog-it/)
* [Les principes SOLID dans la vie de tous les jours](http://www.arolla.fr/blog/2017/02/principes-solid-vie-de-jours/)

## Divers
* [Linus Torvalds remballe les discours sur l’innovation](http://www.silicon.fr/linus-torvalds-remballe-les-discours-sur-linnovation-168971.html)