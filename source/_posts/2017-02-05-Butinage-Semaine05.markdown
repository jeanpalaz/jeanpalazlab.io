---
layout: post
title: "Mes butinages de la semaine 05"
date: 2017-02-06 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraf
tmanship, technical, debt]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 05!

## Artisanat logiciel
* [Java withut IF](http://ashtonkemerling.com/blog/2017/01/26/java-without-if/)
* [Git in 2016](https://hackernoon.com/git-in-2016-fad96ae22a15#.vupmmy0jz)
* [Hype Drivern Development](https://blog.daftcode.pl/hype-driven-development-3469fc2e9b22#.nituglhbl)
* [Hype Drivern Development - part2](https://blog.daftcode.pl/hype-driven-development-part-2-the-phenomenon-68aa63a6d757#.csy53fekk)
* [Functional Programming in Java 8 (Part 0): Motivation](https://dzone.com/articles/functional-programming-in-java-8-part-0-motivation)

## Vie privée
* [nous avons rencontré Max Schrems, l'homme qui fait trembler les géants du Net](http://www.01net.com/actualites/vie-privee-nous-avons-rencontre-max-schrems-l-homme-qui-fait-trembler-les-geants-du-net-1091438.html)
* [Pourquoi sécuriser au maximum le mot de passe de votre boite email ?](https://www.cnil.fr/fr/pourquoi-securiser-au-maximum-le-mot-de-passe-de-votre-boite-email)

## Agilité
* [Planifier pour réussir le sprint](http://www.aubryconseil.com/post/Planifier-pour-reussir-le-sprint)

## Divers
* [Premiers tests avec le machine learning](https://www.miximum.fr/blog/premiers-tests-avec-le-machine-learning/)
* [Un administrateur système de GitLab supprime accidentellement 310 Go de données de production](https://www.developpez.com/actu/115835/Un-administrateur-systeme-de-GitLab-supprime-accidentellement-310-Go-de-donnees-de-production-et-rend-le-site-indisponible-depuis-plusieurs-heures/)